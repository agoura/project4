# Monitoring of Container Based Cluster Environments in the Cloud

## Table of contents
* Prerequisites
* The Fundamentals and the Demo Environment
* Kubernetes Basics
* Monitoring Basics
* Prometheus
* Demo

## Prerequisites
I'm assuming you have a basic understanding of computer virtualization, can distinguish Virtual Machines from Containers, and know how to use a Command Line Interface (CLI), that's it!

You will need:

* The Azure CLI, so we can talk to our Kubernetes nodes
    * [Instructions on installing the Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest)
* An Azure Kubernetes Service (AKS) instance, with a minimum of two nodes
    * Instructions on setting up an AKS instance using the [Azure CLI](https://docs.microsoft.com/en-us/azure/aks/kubernetes-walkthrough) or through the [Azure Portal](https://docs.microsoft.com/en-us/azure/aks/kubernetes-walkthrough-portal)
* The kubectl CLI, so we can manage our Kubernetes cluster
    * [Instructions on installing the kubectl CLI](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* The Helm client, so we can manage packages on our Kubernetes cluster
    * [Instructions on installing the Helm client](https://docs.helm.sh/using_helm/#installing-helm)

I recommend cloning this repository to obtain all source files using:
`git clone https://bitbucket.org/agoura/project4/src/master/`

## The Fundamentals and the Demo Environment

With the development of the [Web 2.0](https://en.wikipedia.org/wiki/Web_2.0), topics such as cloud computing, scaling, high availability, etc., became increasingly hot topics. With corporations (or small start-ups!) offering products serving millions of users, came a need for an unprecedented amount of compute and storage, and the traditional system administration paradigm came under pressure. It was a model that didn't scale very well.

The need for automation became increasingly important, and concepts such as DevOps, Orchestration and Continuous Integration, among many others, were introduced. The way we were thinking about infrastructure changed drastically as well, being able to "rent" compute and storage resources from Cloud Providers such as Amazon Web Services and Microsoft Azure, caught on. Not much later, containers were introduced, changing how products and services were developed, tested and deployed.

This research project was inspired by the company I'll be interning at next semester, Stylelabs runs all of their infrastructure on Azure. For this reason, the cloud platform of choice for this project is Azure. All big cloud providers provide Managed Kubernetes instances, and this proof of concept should roughly (if not fully) behave the same on all these platforms.

---

## Kubernetes Basics
There are containers, and there's container orchestration. Simply put, container orchestration is the art of juggling a lot of different containers and managing container to container communication. All of this keeping automation and scalability in mind.

Because we chose an Azure Kubernetes Service as the orchestrator for this project, we'll need to go over some Kubernetes concepts and terminology, in order for you to get a complete understanding of what we'll be doing later on in the demo. There's no need to do a deep dive, but you'll need to understand which components exist, and how they interact with each other.

### Kubernetes Components

#### Cluster
A cluster is a collection of compute, storage and networking resources that Kubernetes can utilize to run your solution.

### Master Components
Master components provide the cluster's control plane. They are the decision makers that detect and respond to cluster events. E.g. starting up a new pod when a replica requirement is unsatisfied. These master components can be run on any machine in the cluster. For simplicity sake, these scripts are often run on the same machine though. On our Azure Kubernetes Service, most of the master services are abstracted and we won't need to manage them.

#### Node
A node is a single host in your cluster. It could be a physical machine, or in the case of this project, a virtual machine hosted on the cloud. It's job is to facilate Kubernetes components, such as pods. Each node in your cluster will always run some Kubernetes services, such as kube-proxy (responsible for TCP, UDP and SCTP forwarding) and kubelet (the pod scheduler).

#### Pod
A pod is the smallest deployable object in the Kubernetes universe. Each pod contains at least one container,  but can have multiple. Since a pod is an object running on a node, encapsulated containers in a pod will always run on the same node.

#### Service
A service is used to expose pods. Services determine and regulate to which associated pods requests are sent.

#### Namespaces
A namespace is essentially a virtual cluster consisting solely of Kubernetes objects. They enable you to compartmentalize your physical or cloud cluster into multiple virtual ones. Each namespace is completely isolated from the other namespaces, and they can only communicate through public interfaces.

The only objects that run outside of a namespace are node objects, and persistent volumes. Respectively, pods running in different namespaces can still connect to the same volume.

![Kubernetes Diagram](https://bitbucket.org/agoura/project4/raw/69e58c28023a812f8b5d46cc684449e5acfcff1e/images/k8s-layout.PNG "Kubernetes Diagram")

---

## Monitoring Basics

### Basic Monitoring Concepts
Being able to monitor the state of your infrastructure is integral to running a successful operation. Data on the health and performance of your deployments enables you to react to issues, but also to confidently make changes to systems.

A monitoring system will generally consist of the following three pillars:
  1. Producing metrics,
  2. monitoring that provides insights into the health of your systems,
  3. an alerting mechanism.

#### Metrics
Metrics, in essence, are raw data that can be observed and gathered throughout the components in your system. A component could be anything in this case, ranging from a single container, a service running on that container, or the host running in a cluster. Even your cluster as a whole.

Many operating systems and applications (think web and database servers) already provide mechanisms to extract these metrics, such as CPU load and disk space, or requests per minute for a web server. As such, your metrics may range from low-level operating system data, to high-level application specific data.

When aggregated, these metrics enable analysis, and this is where the monitoring system comes in.

#### Monitoring Systems
While metrics are the raw data representing your various components, they are collected and aggregated into a monitoring system that opens it up to analysis. The monitoring system is responsible for storage and visualization of the collected metrics. Especially storage is an important factor, since historical data, once organized, enables you to create context about changes and trends. This is often done through graphs and dashboards.

Last but not least, the monitoring system can define triggers that activate alerts. Whilst interactive dashboards are very useful, alerts allow you to disconnect from having to actively monitor the system yourself.

#### Alerting
An alert is composed of two components, a metrics-based condition, and an action to perform. You essentially define situations that require human interaction with the system, for instance when a deployment consistently fails to build.

Most alerting mechanisms will apply a scale that indicates the severity of an alert and take appropriate action respectively. Storage that reached its 80% threshold might trigger an email alert, whilst a severe increase in database latency might require a text message to the relevant administrator.

### Push versus Pull
A monitoring system generally falls into one of two categories. One approach is the "push" model, where all components push their metrics to a centralized monitoring system using so called "agents". And there's the "pull" approach, where the monitoring system itself goes and collects the metrics. There's a lot of discussion on this topic, mainly concerning the scalability of a model. Scalability of a model depends on how you handle things such as discovery of clients, security, flexibility and overall operational complexity.   

#### Push
Discovery of clients is effortless in a push-based system. Clients come with preconfigured agents that know the location of the monitoring system. They can start sending metrics as soon as they spin up. Security is also relatively simple and the model is inherently secure, as the clients are not listening for any connections. Security could be limited to setting up your firewalls to allow bi-directional traffic between the client and the monitoring system. The push model is relatively inflexible though, since your agents come preconfigured, adding new metrics to monitor later on, is not evident.     

#### Pull
In a pull-based model, discovery requires the monitoring system to do periodic sweeps of the address or namespace to find new clients. This means speed of discovery is dependent on the interval between sweeps, as well as the size of the address or namespace. From a security perspective, pulling data means systems are potentially open to remote access, or denial of service. Pulling data is inherently flexible, you can add any additional metrics to be monitored on the centralized system.

#### In Practice
In almost all environments, a hybrid implementation using both push and pull will be implemented.

---

## Prometheus
Prometheus is an open-source monitoring and alerting system, in development since 2012. In 2016, it was the second project, after Kubernetes, to join the Cloud Native Computing Foundation. This is one of the main reasons I chose to implement Prometheus for this project over more traditional solutions such as Nagios. It fits Kubernetes like a glove for several reasons, such as a label based discovery and the fact that Prometheus is a stateless solution, just like almost every other object running in your Kubernetes cluster.

Prometheus does not collect raw event data, as is done in traditional event-based monitoring. Instead, it aggregates time series data. That means it will collect data only on regular pre-defined intervals, and only collect the current state of a given set of metrics. Sounds pretty vague? I'll elaborate.

`<identifier> ➜ [ (t0, v0), (t1, v1), ... ]` where `t` is your time stamp, and `v` is a value.

Instead of a system sending a message about each HTTP request, it simply counts up those requests in memory, and Prometheus will go and fetch that counter (e.g. every 10 seconds). This means systems could be handling tens of thousands of requests per second, without generating any monitoring traffic. This approach greatly reduces the impact your monitoring system has on things such as your network throughput. This is great line of defense against creating a [DDoS as a Service](https://www.reddit.com/r/devops/comments/9ss2ys/how_to_deal_with_3000tb_of_log_files_daily/
), which is a common pitfall.   

In my opinion, Prometheus pull-based model leans itself best for implementation in a container-based environment because you are centralizing control over what metrics are collected through pulls. Additionally, Prometheus' pull model makes it incredibly easy to make it Highly Available. Simply spin up two Prometheus servers, and tell them what systems to monitor. Yet there's some questions that need exploration. How do you ensure logs and metrics are reliably captured in a pull-based model, knowing that containers can be incredibly ephemeral?   

### How to expose metrics to Prometheus
Exposing Prometheus metrics, also referred to as instrumenting your services, can be done in two ways, either through the use of exporters, or by writing client libraries.

Exporters are sidecar containers you run next to your application containers (in this demo, an Apache web server) in a pod. Exporters take metrics the application generates (in our case, metrics from Apache's /server-status page), translates them to Prometheus metrics, and exposes them on a specific port and path of the pod. There are a lot of [libraries](https://prometheus.io/docs/instrumenting/exporters/) already for popular services available that can help you build exporters.

If you, however, run services that were built in-house, you'll need to instrument your application yourself using client libraries. A client library allows you to define metrics to be tracked and will expose these metrics for Prometheus to pull. This is a relatively complex endeavour and is located more in the realm of DevOps, which is out of scope for this project, so I won't really go into this anymore in this document.    

---

## Demo
Time to get our hands dirty. In this demo we'll be setting up and configuring a managed Azure Kubernetes cluster composed of two nodes. Later on, we'll install a Prometheus instance and create a dashboard that will provide us insights into our operation.

Before starting this demo, make sure you've installed all the tools mentioned in the prerequisites. Last but not least, you'll find all the configuration files I'll be using throughout this demo in the "demoenv" folder in this repository.

### Accessing your cluster
We can use the command below to get the credentials for your cluster, these credentials will be saved locally in your kubectl config.

```
➜  ~ az aks get-credentials --resource-group jdgaks --name jdgaks

Merged "jdgaks" as current context in /home/jonas/.kube/config
```

To verify this worked, you can try to fetch all the nodes in your cluster.

```
➜  ~ kubectl get nodes

NAME                       STATUS   ROLES   AGE     VERSION
aks-nodepool1-13424116-0   Ready    agent   5d20h   v1.9.11
aks-nodepool1-13424116-1   Ready    agent   5d20h   v1.9.11
```

### Deploying an application
Let's go ahead and deploy an application to Kubernetes. We'll be using `kubectl apply -f demoenv/kubedeploys/demowebsite.yaml`.

Let's dissect the YAML file, we're creating 3 objects:
1. A namespace,
2. a deployment, which can be one or more containers, such as a front- and backend, in this case it's just a frontend container serving a PHP website, which gets pulled from my Docker Hub repository,
3. a service, which is an internet-facing load balancer.

```YAML
apiVersion: v1
kind: Namespace
metadata:
  name: demowebsite
  labels:
    name: demowebsite

---

apiVersion: apps/v1
kind: Deployment
metadata:
  name: demowebsite-deployment
  namespace: demowebsite
  labels:
    app: demowebsite
    version: "1.0"
spec:
  replicas: 4
  selector:
    matchLabels:
      app: demowebsite
  template:
    metadata:
      labels:
        app: demowebsite
        version: "1.0"
        server: apache
    spec:
      containers:
      - name: demowebsite-frontend
        image: jonasdegendt/klasmgmt:1.0
        ports:
        - name: web
          containerPort: 80

---

apiVersion: v1
kind: Service
metadata:
  name: demowebsite-lb
  namespace: demowebsite
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: demowebsite
```

### The Kubernetes Dashboard

Let's have a look at the Kubernetes dashboard. You can create a tunnel from your AKS instance to your localhost using this command:
`az aks browse --resource-group jonas-aks --name jonas-aks`

In case nothing pops up, go ahead and browse to http://localhost:8001.

![Kubernetes Dashboard](https://bitbucket.org/agoura/project4/raw/24e1a3b3ad7bd64f8818d63cbe934bdd35c76361/images/kubernetesdashboard.png "Kubernetes Dashboard")

You'll notice right away that the dashboard is already displaying some basic insights, such as CPU and memory usage, we can request the same information through the command line using `kubectl top nodes`.

```
➜  ~ kubectl top nodes

NAME                       CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%   
aks-nodepool1-13424116-0   126m         6%     1337Mi          25%       
aks-nodepool1-13424116-1   92m          4%     1307Mi          25%
```

The same goes for pods, by default, Kubernetes pods will generate logs and output them to stdout, which you can view from the dashboard. This is by no means ideal though, it's impractical to inspect logs by sifting through individual pods. Additionally, pods are ephemeral, this means that once they die, all this output is gone, and it might contain some important pointers on why it died. This is why we need a central system that aggregates these logs so we can preserve them.

![Kubernetes Pod Logs](https://bitbucket.org/agoura/project4/raw/24e1a3b3ad7bd64f8818d63cbe934bdd35c76361/images/kubernetesdashboardlogging.png "Kubernetes Pod Logs")  

### Creating a Monitoring namespace
We'll go ahead and create a monitoring namespace in which we'll be deploying our Prometheus server.
```
kubectl apply -f demoenv/kubedeploys/monitoring-namespace.yaml
```

```YAML
apiVersion: v1
kind: Namespace
metadata:
  name: monitoring
  labels:
    name: monitoring
```

### Installing prometheus-operator and kube-prometheus using Helm
Helm is a package manager for your Kubernetes instance. It helps you install and manage applications, such as kube-prometheus, on your Kubernetes cluster.

First of all, we'll need to create a service account on our Kubernetes cluster that allows Helm to make changes to it.

`kubectl apply -f demoenv/kubedeploys/helm-rbac.yaml`
`helm init --service-account tiller`

```YAML
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
```

The first parameter for our Helm install is the repository and package to fetch, followed by the internal name, and the namespace where we'll be doing our installation.
```
helm repo add coreos https://s3-eu-west-1.amazonaws.com/coreos-charts/stable/
helm install coreos/prometheus-operator --name prometheus-operator --namespace monitoring
helm install coreos/kube-prometheus --name kube-prometheus --namespace monitoring
```

I'm sure you've noticed we installed two packages, prometheus-operator, and kube-prometheus. What's an operator, you're asking?

>An operator is a method of packaging, deploying and managing a Kubernetes application. A Kubernetes application is an application that is both deployed on Kubernetes and managed using the Kubernetes APIs and kubectl tooling. To be able to make the most of Kubernetes, you need a set of cohesive APIs to extend in order to service and manage your applications that run on Kubernetes. You can think of Operators as the runtime that manages this type of application on Kubernetes.

An operator deploys a set of resources (pods, services, ...) that form an application, and then makes that application manageable through the kubeAPI and kubectl, in this case, a Prometheus environment. Through leveraging a Kubernetes feature called [Custom Resource Definitions](https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definitions), the prometheus-operator also creates a brand new resource type called a service monitor. The prometheus-operator watches the Kubernetes namespace for these service monitors, and when it detects one was created, it will configure the Prometheus server to scrape resources according to how you defined the service monitor.

For monitoring nodes, it would work like this:

![prom-operator-diagram](https://bitbucket.org/agoura/project4/raw/e003b67908a92aca3b1db39da507080ee7f0f65f/images/prom-operator-diagram.png "Prom Operator Diagram")

Let's go ahead and inspect some of these objects. If we go ahead and execute `kubectl get prometheus --all-namespaces` we should be seeing our Prometheus deployment.
```
➜  ~ kubectl get prometheus --all-namespaces -l release=kube-prometheus

NAMESPACE    NAME
monitoring   kube-prometheus
```

The `kube-prometheus-exporter-node` defines Prometheus should be monitoring our nodes.
```
➜  ~ kubectl get servicemonitor kube-prometheus-exporter-node --namespace=monitoring

NAME                                               AGE
kube-prometheus-exporter-node                      2d
```

This means we need some exporter pods that are exposing metrics though, and sure thing, they exist.
```
➜  ~ kubectl get pods --namespace=monitoring                           

NAME                                                  READY   STATUS    RESTARTS   AGE
kube-prometheus-exporter-node-fzd6b                   1/1     Running   5          2d
kube-prometheus-exporter-node-rwpkt                   1/1     Running   5          2d
```

Last but not least, there should be a service exposing these pods, in this case, on port 9100.
```
➜  ~ kubectl get services --namespace=monitoring       
NAME                                  TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)             AGE
kube-prometheus-exporter-node         ClusterIP   10.0.211.202   <none>        9100/TCP            2d
```

That about sums up all the important components for monitoring of our nodes.

### Accessing Prometheus
Prometheus offers a very basic dashboard you can connect to and use to query the collected metrics, let's go ahead and query some metrics from our nodes. Prometheus queries are done through their very own language called, you guessed it, Prometheus Query Language (PQL). Queries can be visualized using tables or graphs.

```
kubectl --namespace monitoring port-forward $(kubectl get pod --namespace monitoring -l prometheus=kube-prometheus -l app=prometheus -o template --template "{{(index .items 0).metadata.name}}") 9090:9090
```
Go ahead and browse to http://localhost:9090/graph.

### PromQL

The PromQL in itself is relatively complex, it has an extensive amount of functions (think `count()`) and operators (+, -, %) you can use. Let's start with a basic query to check if our nodes are up and running.

Through the power of labels, we'll be able to query our nodes by simply nesting `job="node-exporter"` in a PQL function called `up`, so it becomes `up{job="node-exporter"}`. If we run this query we'll get two `1` results, meaning our nodes are up and running.

![PromQL Up Function](https://bitbucket.org/agoura/project4/raw/1ca057b2f877ca2b93656d8a92f94be650eb7607/images/promQL-up.png "PromQL Up Function")

Something more complex would be querying available memory on a node. To make sure we get an accurate number, we'll go ahead and query the total memory of the node, and deduct the free memory, as well as the memory cache and buffer.
`node_memory_MemTotal{instance="10.240.0.4:9100"} - node_memory_MemFree{instance="10.240.0.4:9100"} - node_memory_Buffers{instance="10.240.0.4:9100"} - node_memory_Cached{instance="10.240.0.4:9100"}`

![PromQL Memory Available Graph](https://bitbucket.org/agoura/project4/raw/49fccacb00e97537340623b1c4007fa7047730c1/images/promQL-memavailable.png "PromQL Memory Available Graph")

### Instrumenting our web server
We've gone over monitoring of our nodes, and querying and graphing metrics using PQL, so it's time to instrument the web server we deployed earlier on in the demo.

We'll be adding a second container to our pod. This is a sidecar container that contains an Apache exporter written in Go, it grabs metrics from our Apache's `/server-status` page, and translates them to Prometheus metrics. Because of how Kubernetes is set up, containers in a pod share the same localhost, so we don't have to take care of any kind of discovery, not meddle with credentials.  

We'll add another container definition to our YAML file from earlier. Once again, this image is pulled from my Docker Hub repository. We'll open up port 9117, and call it `metrics`.
```YAML
- name: demowebsite-exporter
  image: jonasdegendt/apacheexporter:1.0
  ports:
  - name: metrics
    containerPort: 9117
```

At this point, we'll go ahead and run `kubectl apply -f demoenv/kubedeploys/demowebsite-instrumented.yaml`, and Kubernetes will automatically update our earlier deployment with the extra exporter containers. This is done through what is called a rolling update, Kubernetes will gradually deploy new pods, and gradually terminate the old version, to prevent service disruption.

I also added an additional `nodeport` service to the YAML file, this service exposes the exporter on each Node’s IP at a static port (9117). Last but not least, we'll need to tell Prometheus to scrape this service. As explained earlier on, this is done through creating a service monitor. Once the prometheus-operator detects our service operator deployment, it will look at it's definitions, and configure the Prometheus server accordingly.

Go ahead and run `kubectl apply -f demoenv/kubedeploys/demowebsite-servicemonitor.yaml`, let's dissect the file.

We're deploying a service monitor, as indicated by the label `kind: ServiceMonitor`. We give the monitor a name and tell it in which namespace it lives (`monitoring`), we'll also define some labels. In the `spec` section of our deployment we assign the interval for scrapes, and which port Prometheus should use for pull requests. Note that we're using the name of the port to do this, not the port number. If we were to change the port number of our exporter container later on, we wouldn't be breaking this service monitor. Last but not least, we define in which namespace the exporter service resides, as well as it's name (`app: exporter-demowebsite`).

```YAML
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: kube-prometheus-exporter-demowebsite
  namespace: monitoring
  labels:
    app: exporter-demowebsite
    component: demowebsite-exporter
    prometheus: kube-prometheus
    release: kube-prometheus
spec:
  endpoints:
  - interval: 15s
    port: metrics
  jobLabel: component
  namespaceSelector:
    matchNames:
      - demowebsite
  selector:
    matchLabels:
      app: exporter-demowebsite
      component: demowebsite-exporter
```

### Inspecting our web server metrics
At this point Prometheus will start scraping our exporter for Apache metrics, and we'll be able to query stuff such as `apache_cpu_load` and `apache_sent_kilobytes_total`.

![PromQL Apache CPU Load](https://bitbucket.org/agoura/project4/raw/6618ea5f98ba8c58cd3d725bea81d59ba2fd0a41/images/promQL-apache-cpu-load.png "PromQL Apache CPU Load")

### Alerting
Now that we have metrics on our nodes and web servers, we can set up alerting. Just like service monitors, this is done through a Custom Resource Definition, called a PrometheusRule. When the prometheus-operator detects a PrometheusRule deployment, it will configure the AlertManager respectively.

A PrometheusRule can container multiple alerts, ordered in groups. An alert consists of a collection of parameters, such as a name, expression, severity and labels.

An expression is essentially a PQL query. For example, `avg(up{job="node-exporter"}) BY (job) < 0.75 for 10m`, in this query we collect all running nodes using `up{job="node-exporter"}`. We average this value over 10 minutes to make sure we don't report a node is dead based on a temporary network issue.

We can apply the same logic for both nodes being down using `avg(up{job="node-exporter"}) BY (job) = 0 for 5m`.

```YAML
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    prometheus: kube-prometheus
    role: alert-rules
  name: prometheus-node-down-rule
  namespace: monitoring
spec:
  groups:
  - name: nodes-down-rule
    rules:
    - alert: OneNodeDown
      expr: avg(up{job="node-exporter"}) BY (job) < 0.75
      for: 10m
      labels:
        severity: warning
      annotations:
        description: One of our nodes is down
    - alert: AllNodesDown
      expr: avg(up{job="node-exporter"}) BY (job) = 0
      for: 5m
      labels:
        severity: critical
      annotations:
        description: All nodes are down
```

Unfortunately I don't own an e-mail server, but configuring alert receivers isn't terribly hard, a configuration file would look something like this.

```YAML
global:
  smtp_smarthost: 'localhost:25'
  smtp_from: 'alertmanager@example.org'
  smtp_auth_username: 'alertmanager'
  smtp_auth_password: 'password'

receivers:
  - name: 'frontend-team'
    email_configs:
  - to: 'frontend-team@example.org'
```

## Conclusion
This document should have given you some insights in how Kubernetes, monitoring and Prometheus works. The demo enabled you to get some hands-on experience, and I hope you'll be able to apply this knowledge to any (future) projects you are working on.

Sources for this project can be found [here](https://bitbucket.org/agoura/project4/src/master/sources).
