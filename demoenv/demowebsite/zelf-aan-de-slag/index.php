<!DOCTYPE html>
<html>
<head>
	<title>Politieke Organisatiestructuren</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

	<header>
		<nav>
			<ul>
				<li><a href="/">Start</a></li>
				<li><a href="/organisatiestructuren/">Organisatiestructuren</a></li>
				<li><a href="/onze-situatie/">Onze situatie</a></li>
				<li><a href="/verkiezingen/">Verkiezingen</a></li>
				<li><a href="/zelf-aan-de-slag/">Zelf aan de slag</a></li>
				<li><a href="/bijlagen/">Bijlagen</a></li>
				<li><a href="/help/">Help</a></li>
			</ul>
		</nav>
		<h1>Zelf aan de slag</h1>
	</header>

	<div class="container" id="intro">
		<div class="row">
			<div class="col-sm-12 block block-small-bottom">
				<h1>Hoe gaan jullie te werk?</h1>
				<p>Jullie vormen zelf partijgroepen van ongeveer 4 leerlingen. Jullie zijn zelf verantwoordelijk om een eigen politieke partij op te richten dat haalbare programmapunten (maximum 5) naar voor schuift. Denk er aan dat deze programmapunten als inspiratiebron voor mogelijke implementatie bij de directie terecht komen wanneer jullie de verkiezingen ‘winnen’. Wees creatief, maar niet overambitieus. 
<br/><br/>
Het is de bedoeling dat deze programmapunten jullie doelpubliek bereiken (= de volledige derde graad van de school en het leraren-en directiekorps). Zij stemmen eind mei voor hun favoriete partij. Denk goed na over hoe deze kiezers het best te bereiken zijn en wees creatief met de communicatiemiddelen. Kortom: jullie moeten minstens een maand campagne voeren want een partij waarvan de kiezers het bestaan niet afweten, kan onmogelijk iets verwezenlijken.</p>

<h1>Het stappenplan</h1>






<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <p class="mb-0">Stap 1: Verdeling in +- gelijke groepen door de leerlingen.
      </p>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Stap 2: Opstarten logboek
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        Elke keer jullie samen komen of iets doen in het kader van de verkiezingen noteren jullie dit. Je kan een sjabloon downloaden onder ‘bijlagen’.<br/>
        Brainstorm over partijpunten en zorg voor eensgezindheid binnen de partij.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <p class="mb-0">Stap 3: Zoek een naam voor jullie partij
      </p>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFour">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          Stap 4: Opstellen kandidatenlijst (trekker- en duwer)
        </button>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
      <div class="card-body">
        Wie wordt lijsttrekker of het gezicht van de partij? Wie wordt lijstduwer of het brein van de partij?
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFive">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          Stap 5: Ontwikkeling verkiezingsprogramma + titel
        </button>
      </h5>
    </div>
    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
      <div class="card-body">
        Ontwikkel een verkiezingsprogramma waarin je aanhaalt wat je wil bereiken als partij.  Schrijf een verkiezingsprogramma van ongeveer één A4 en bedenk een goede titel voor jullie programma.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <p class="mb-0">Stap 6: Test het verkiezingsprogramma op haalbaarheid.
      </p>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingSeven">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
          Stap 7: Zoek middelen om campagne te voeren
        </button>
      </h5>
    </div>
    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
      <div class="card-body">
        Zoek een middel om over jouw partij en partijpunten te communiceren. Ga op zoek naar manieren om actief campagne te voeren (hiervoor kan je eerst wat research doen op het internet of reflecteren over hoe het er in jouw gemeente aan toe gaat).
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <p class="mb-0">Stap 8: Blijf gedurende de periode actief bezig met campagne voeren
      </p>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingNine">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
          Stap 9: Verkiezingsdag (zelf stemmen)
        </button>
      </h5>
    </div>
    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
      <div class="card-body">
        Als klas zorgen jullie ervoor dat het lokaal ingekleed wordt met kieshokjes. De stemmen worden door twee leerkrachten verzameld en nageteld. Jullie gaan zelf ook stemmen. Maandag na de verkiezingen wordt de winnende partij bekend gemaakt via smartschool.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTen">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
          Stap 10: Vorm een coalitie
        </button>
      </h5>
    </div>
    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample">
      <div class="card-body">
        <p>Nu jullie hebben gestemd is het belangrijk dat jullie kijken welke partijen samen voldoende draagvlak hebben om een coalitie te vormen. Twee leerlingen vervullen de rol van informateur. De informateurs moeten niet in de vertegenwoordiging zitten. Deze worden door de leerkracht gekozen. In realiteit worden deze informateurs door de koning aangesteld. Deze informateurs moeten partijen vinden die samen een meerderheid van de stemmen hebben behaald en die willen samenwerken en besluiten durven nemen.</p>

        <table class="table table-bordered">
	<thead>
		<tr>
      <th scope="col" width="50%" class="text-center">AFZONDERLIJKE PARTIJ</th>
      <th scope="col" width="50%" class="text-center">FORMATEURS</th>
    </tr>
	</thead>
	  <tbody>
    <tr>
      <td scope="row">
      	<ol>
      		<li>Kies met je partijgenoten drie plannen of ideeën die jullie graag willen uitvoeren. Geef een waardering aan deze plannen. Welk idee vind je het belangrijkst? Geef dit idee drie punten. Schrijf dit op een apart briefje. Welk plan vind je daarna het belangrijkst? Geef dit plan twee punten. Geef het overgebleven plan één punt. </li>
      		<li>Hoe gaan jullie straks onderhandelen? Bedenk of jullie veel of weinig eisen kunnen stellen en hoe graag je straks mee beslist in een coalitie. Als je veel stemmen hebt gehaald bij de verkiezingen, dan kun je misschien meer eisen.</li>
      	</ol>
      </td>
      <td>
      	<ol>
      		<li>Welke partijen kunnen samen rekenen op meer dan de helft van de zetels?</li>
      		<li>Welke partijen kunnen waarschijnlijk goed samenwerken? Met andere woorden: welke coalitie vinden jullie logisch? Bepaal met welke partijen jullie het eerst willen praten over een samenwerking.</li>
      		<li>De informateurs gaan nu aan de slag. Zij vragen verschillende partijen aan de onderhandelingstafel.</li>
      	</ol>
      </td>
    </tr>
    <tr>
      <th scope="row" colspan="2" class="text-center">DE ONDERHANDELING: FORMATEURS EN DE PARTIJEN</th>
    </tr>
    <tr>
      <td scope="row" colspan="2">
      	<p>De informateurs: zorg dat je drie blanco kaartjes hebt.  Vertel de partijen die je uitnodigt aan de onderhandelingstafel dat er een akkoord van drie plannen uit de onderhandelingen moet komen. Na een kwartier moeten jullie een akkoord met partijen hebben bereikt. Vul dan de drie kaartjes in met de plannen die de coalitie  heeft goedgekeurd. Er is tijdsdruk: jullie hebben 15 minuten de tijd om een coalitie te regelen. Houd dus goed de leiding tijdens de onderhandelingen.</p>
      </td>
    </tr>
  </tbody>
</table>


      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingEleven">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
Stap 11: Stel een regeerakkoord op
        </button>
      </h5>
    </div>
    <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordionExample">
      <div class="card-body">
        Stel een regeerakkoord op, volgend uit de onderhandeling. Geef het akkoord een titel en stel het voor aan de rest van de klas. De lijsttrekker van de grootste partij wordt minister-president en verdeelt de verantwoordelijkheden ter realisatie van de plannen. Op dit moment vormen de aangestelde leerlingen een regering. Is dit gelukt? Dan mogen deze mensen en de lijsttrekker van de grootste coalitiepartij met de docent op de foto!
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwelve">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
          Stap 12: Schrijf een tweedelige reflectie over enerzijds de afgelopen verkiezingen<br/>en anderzijds over democratie in het algemeen
        </button>
      </h5>
    </div>
    <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordionExample">
      <div class="card-body">
        Elke lid van de partij reflecteert over de verkiezingen, de eigen partij en de partijpunten. Ga na waarom je slecht/goed scoorde bij de kiezers en geef ook aan wat je van het verkiezingssysteem, onze staatsstructuur en democratie denkt. Verkies je onze staatsstructuur of vind je andere vormen beter? Onderbouw je mening. Je taak is dus tweedelig:
- Deel 1: reflecteren over de verkiezingen
- Deel 2: reflecteren over de algemene staatsstructuur en democratie
De verkiezingen (12p) en je reflectie (8p) staan op 20 punten van het eindexamen voor cultuurwetenschappen. Je krijgt geen punten op het winnen of verliezen, maar wel op de aanpak, inzet en motivering ervan.
      </div>
    </div>
  </div>
</div>


<a href="/bijlagen/" class="btn btn-primary" style="margin-top: 30px;">Bijlagen</a>

			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
	<script type="text/javascript">
		$( window ).on( "load", function() {
		// $("#kaart iframe #screenshot").trigger("click");
		// var $iframe = $("#maphub").contents();
		// $iframe.find('#screenshot').trigger("click");
	})
</script>
</body>
</html>