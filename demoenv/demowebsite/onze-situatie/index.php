<!DOCTYPE html>
<html>
<head>
	<title>Politieke Organisatiestructuren</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

	<header>
		<nav>
			<ul>
				<li><a href="/">Start</a></li>
				<li><a href="/organisatiestructuren/">Organisatiestructuren</a></li>
				<li><a href="/onze-situatie/">Onze situatie</a></li>
				<li><a href="/verkiezingen/">Verkiezingen</a></li>
				<li><a href="/zelf-aan-de-slag/">Zelf aan de slag</a></li>
				<li><a href="/bijlagen/">Bijlagen</a></li>
				<li><a href="/help/">Help</a></li>
			</ul>
		</nav>
		<h1>Onze situatie</h1>
	</header>

	<div class="container" id="intro">
		<div class="row">
			<div class="col-sm-12 block block-small-bottom">
				<h1>Opdracht</h1>
				<p>Bekijk het fragment over democratie in Nederland en probeer naar analogie de kernbegrippen in te vullen in onderstaande tekst:</p>
			</div>
		</div>
	</div>

	<div class="container" id="video">
		<div class="row">
			<div class="col-sm-12">

				<div class="videoWrapper"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/gOSPJRB9rk0?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>

			</div>
		</div>
	</div>
	

	<div class="container" id="opdracht">
		<div class="row">
			<div class="col-sm-12 block block-small-bottom">
				<div class="alert alert-secondary" role="alert">
					persvrijheid - wetgevende, uitvoerende en rechterlijke macht - de koning, de regering van de ministers en het parlement - fundamentele vrijheden en rechten - democratische - vrije en eerlijke verkiezingen - eensgezindheid - parlement - grondwet - tijdelijk - besluitvorming - recht om zich te verenigen - meerderheid - verdeeld - trias politica - vrije meningsuiting -  bestuursvorm - oppositie - rechtsstaat - volksvertegenwoordigers 
				</div>
				<form id="formOpdracht" class="form-inline">
					<p>Wij leven in een constitutionele parlementaire monarchie, waar de macht verspreid is tussen <input data-text="de koning" type="text" class="form-control" />, <input data-text="de regering van de ministers en het parlement" type="text" class="form-control" />. Binnen dergelijke monarchie staat de <input data-text="democratische" type="text" class="form-control" /> gedachte centraal. Democratie betekent letterlijk dat het volk de macht uitoefent. Het is een <input data-text="bestuursvorm" type="text" class="form-control" /> waarbij het volk regeert, maar omdat we om praktische redenen niet allemaal kunnen mee besturen, zijn de meeste democratieën representatieve democratieën. In praktijk verloopt dit via een systeem van <input data-text="volksvertegenwoordiging" type="text" class="form-control" /> in het <input data-text="parlement" type="text" class="form-control" />. 
						<br/><br/>
						Het is niet gemakkelijk om het begrip democratie uit te leggen omdat er wereldwijd verschillende soorten democratieën bestaan. Ze worden allen democratisch genoemd, maar kunnen enorm van elkaar verschillen in concrete werkwijze. Toch zijn er bepaalde kenmerken die in elke democratie horen terug te komen. 

						In een democratie is de macht altijd <input data-text="tijdelijk" type="text" class="form-control" /> en is deze <input data-text="verdeeld" type="text" class="form-control" /> over drie verschillende machten. Een andere benaming is de <input data-text="trias politica" type="text" class="form-control" />, waar macht gedecentraliseerd wordt tussen de <input data-text="wetgevende, uitvoerende en rechtelijke macht" type="text" class="form-control" />. Deze macht wordt door de bevolking verleend aan <input data-text="volksvertegenwoordigers" type="text" class="form-control" /> die via <input data-text="vrije en eerlijke verkiezingen" type="text" class="form-control" /> verkozen worden. In theorie blijft men aan de macht tot de volgende verkiezingen.</p>


						<div class="row block-small w-100">
							<div class="col-sm-4">
								<div class="card">
									<div class="card-header">
										WM
									</div>
									<ul class="list-group list-group-flush">
										<li class="list-group-item">Parlementen</li>
										<li class="list-group-item">Maken wetten</li>
									</ul>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="card">
									<div class="card-header">
										UM
									</div>
									<ul class="list-group list-group-flush">
										<li class="list-group-item">Regeringen</li>
										<li class="list-group-item">Voeren wetten uit</li>
										<li class="list-group-item">Stellen wetten voor</li>
									</ul>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="card">
									<div class="card-header">
										RM
									</div>
									<ul class="list-group list-group-flush">
										<li class="list-group-item">Onafhankelijke rechters</li>
										<li class="list-group-item">Controle over naleving wetten</li>
									</ul>
								</div>
							</div>
						</div>


						<p>
							Het is belangrijk om te onthouden dat binnen een democratie de macht op basis van een <input data-text="meerderheid" type="text" class="form-control" /> wordt uitgeoefend. Daarbovenop moeten de <input data-text="fundamentele vrijheden en rechten" type="text" class="form-control" /> van de burgers formeel erkend, beschermd en gewaarborgd worden om te kunnen spreken van een échte democratie. Voorbeelden hiervan zijn het recht op <input data-text="vrije meningsuiting" type="text" class="form-control" />, <input data-text="persvrijheid" type="text" class="form-control" /> en het <input data-text="recht om zich te verenigen" type="text" class="form-control" />. Deze vrijheden en rechten zorgen ervoor dat de burgers invloed kunnen uitoefenen en kritiek kunnen uiten op de <input data-text="besluitvormig" type="text" class="form-control" />. In dictaturen is dit niet het geval. De belangrijkste regels staan in de <input data-text="grondwet" type="text" class="form-control" />. 
							<br/><br/>
							Een <input data-text="rechtsstaat" type="text" class="form-control" /> is eigen aan een democratie. We kunnen niet van een geslaagde democratie spreken als regels niet nageleefd worden. Ze worden opgelegd en worden afgedwongen. Niet-naleving wordt opgevolgd door de rechtelijke macht. 
							<br/><br/>
							Desondanks dat het “volk regeert” is er binnen een democratie bijna nooit unanieme <input data-text="eensgezindheid" type="text" class="form-control" />. Zoals eerder gesteld wordt de macht uitgeoefend door de meerderheid, waar een <input data-text="oppositie" type="text" class="form-control" /> tegenover staat. Daardoor zijn pluralisme, meningsverschillen en conflicten eigen aan een democratie.
						</p>


					</form>
					<div id="opdrachtMelding" class="alert alert-success" role="alert" style="display: none;">Volledig juist, proficiat!</div>

						<button id="btnCheckOpdracht" type="submit" class="btn btn-primary">Controleren</button>
						<a id="afterOpdracht" href="/verkiezingen/" class="btn btn-primary margin-left" style="display: none;">Volgende</a>
				</div>
			</div>

		</div>

		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
		<script type="text/javascript">
			$( document ).ready(function() {

			// 	$("#formOpdracht input").each(function(){
			// 		var input = $(this)[0];
			// 		var answer = $(input).data("text");
			// 	// console.log(answer);
			// 	$(input).val(answer);
			// });


				$("#btnCheckOpdracht").click(function(e){
					e.preventDefault();
					var melding = "";
					$("#formOpdracht input").each(function(){
						var input = $(this)[0];
						var answer = $(input).data("text");
						var typed = $(input).val();

						if(answer == typed){
							// console.log(typed);
							// $(input).addClass("val-ok");
						}else{
							melding = "Er is was iets verkeerd, bekijk de juiste oplossing en ga verder.";
							$(input).addClass("val-nok");
							// $(input).after('<div class="invalid-feedback">Fout, juiste antwoord: '+answer+'</div>');
							$(input).val(answer);
						}

					});

					if(melding != ""){
						$("#opdrachtMelding").removeClass('alert-success');
						$("#opdrachtMelding").addClass('alert-warning');
						$("#opdrachtMelding").html(melding);
					}
					$("#opdrachtMelding").show();
					$("#afterOpdracht").show();

				});
			})
		</script>
	</body>
	</html>