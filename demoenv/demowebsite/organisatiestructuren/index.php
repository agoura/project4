<!DOCTYPE html>
<html>
<head>
	<title>Politieke Organisatiestructuren</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

	<header>
		<nav>
			<ul>
				<li><a href="/">Start</a></li>
				<li><a href="/organisatiestructuren/">Organisatiestructuren</a></li>
				<li><a href="/onze-situatie/">Onze situatie</a></li>
				<li><a href="/verkiezingen/">Verkiezingen</a></li>
				<li><a href="/zelf-aan-de-slag/">Zelf aan de slag</a></li>
				<li><a href="/bijlagen/">Bijlagen</a></li>
				<li><a href="/help/">Help</a></li>
			</ul>
		</nav>
		<h1>Organisatiestructuren</h1>
	</header>

	<div class="container" id="intro">
		<div class="row">
			<div class="col-sm-12 block block-small-bottom">
				<h1>Interactieve kaart</h1>
				<p>Hieronder vinden jullie een wereldkaart waar politieke organisatiestructuren via een reis rond de wereld kunnen ontdekt worden. Ga zelf aan de slag door de verschillende structuren te bestuderen.</p>
			</div>
		</div>
	</div>

	<div id="kaart">
		<!-- <iframe id="maphub" width="100%" height="800" scrolling="no" seamless="seamless" src="https://maphub.net/embed/29203" frameborder="0" allowfullscreen></iframe> -->
		<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1GS-6vBHiv2H0xEt0D8f9QnqHiM6c00Nq" width="100%" height="800"></iframe>
	</div>

	<div class="container" id="quiz">
		<div class="row">
			<div class="col-sm-12 block block-small-bottom">
				<h1>Quiz</h1>
				<p>Jullie zijn terug van jullie wereldreis. Test de kennis uit de interactieve kaart in de quiz hieronder.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 block-bottom">
				<form>
				<div class="row quiz-row">
					<div class="col-sm-3 quiz-flag d-flex align-items-center">
						<img src="/images/flags/italy.png" width="64" /><h3>Italië</h3>
					</div>
					<div class="col-sm-9 quiz-sel">
						<div class="form-group">
							<label for="sel_italy">Kies de juiste organisatiestructuur</label>
							<select class="form-control" id="sel_italy" data-answer="Parlementaire republiek">
								<option>Constitutionele monarchie</option>
								<option>Presidentiële republiek</option>
								<option>Dictatuur</option>
								<option>Absolute monarchie</option>
								<option>Theocratie</option>
								<option>Parlementaire republiek</option>
								<option>Mislukte staat</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row quiz-row">
					<div class="col-sm-3 quiz-flag d-flex align-items-center">
						<img src="/images/flags/spain.png" width="64" /><h3>Spanje (onder Franco)</h3>
					</div>
					<div class="col-sm-9 quiz-sel">
						<div class="form-group">
							<label for="sel_spain">Kies de juiste organisatiestructuur</label>
							<select class="form-control" id="sel_spain" data-answer="Dictatuur">
								<option>Constitutionele monarchie</option>
								<option>Presidentiële republiek</option>
								<option>Dictatuur</option>
								<option>Absolute monarchie</option>
								<option>Theocratie</option>
								<option>Parlementaire republiek</option>
								<option>Mislukte staat</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row quiz-row">
					<div class="col-sm-3 quiz-flag d-flex align-items-center">
						<img src="/images/flags/france.png" width="64" /><h3>Frankrijk</h3>
					</div>
					<div class="col-sm-9 quiz-sel">
						<div class="form-group">
							<label for="sel_france">Kies de juiste organisatiestructuur</label>
							<select class="form-control" id="sel_france" data-answer="Absolute monarchie">
								<option>Constitutionele monarchie</option>
								<option>Presidentiële republiek</option>
								<option>Dictatuur</option>
								<option>Absolute monarchie</option>
								<option>Theocratie</option>
								<option>Parlementaire republiek</option>
								<option>Mislukte staat</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row quiz-row">
					<div class="col-sm-3 quiz-flag d-flex align-items-center">
						<img src="/images/flags/wit-rusland.png" width="64" /><h3>Wit-Rusland</h3>
					</div>
					<div class="col-sm-9 quiz-sel">
						<div class="form-group">
							<label for="sel_wr">Kies de juiste organisatiestructuur</label>
							<select class="form-control" id="sel_wr" data-answer="Presidentiële republiek">
								<option>Constitutionele monarchie</option>
								<option>Presidentiële republiek</option>
								<option>Dictatuur</option>
								<option>Absolute monarchie</option>
								<option>Theocratie</option>
								<option>Parlementaire republiek</option>
								<option>Mislukte staat</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row quiz-row">
					<div class="col-sm-3 quiz-flag d-flex align-items-center">
						<img src="/images/flags/iran.png" width="64" /><h3>Iran</h3>
					</div>
					<div class="col-sm-9 quiz-sel">
						<div class="form-group">
							<label for="sel_iran">Kies de juiste organisatiestructuur</label>
							<select class="form-control" id="sel_iran" data-answer="Theocratie">
								<option>Constitutionele monarchie</option>
								<option>Presidentiële republiek</option>
								<option>Dictatuur</option>
								<option>Absolute monarchie</option>
								<option>Theocratie</option>
								<option>Parlementaire republiek</option>
								<option>Mislukte staat</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row quiz-row">
					<div class="col-sm-3 quiz-flag d-flex align-items-center">
						<img src="/images/flags/tsjaad.png" width="64" /><h3>Tsjaad</h3>
					</div>
					<div class="col-sm-9 quiz-sel">
						<div class="form-group">
							<label for="sel_tsjaad">Kies de juiste organisatiestructuur</label>
							<select class="form-control" id="sel_tsjaad" data-answer="Mislukte staat">
								<option>Constitutionele monarchie</option>
								<option>Presidentiële republiek</option>
								<option>Dictatuur</option>
								<option>Absolute monarchie</option>
								<option>Theocratie</option>
								<option>Parlementaire republiek</option>
								<option>Mislukte staat</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row quiz-row">
					<div class="col-sm-3 quiz-flag d-flex align-items-center">
						<img src="/images/flags/uk.png" width="64" /><h3>Verenigd Koninkrijk</h3>
					</div>
					<div class="col-sm-9 quiz-sel">
						<div class="form-group">
							<label for="sel_uk">Kies de juiste organisatiestructuur</label>
							<select class="form-control" id="sel_uk" data-answer="Constitutionele monarchie">
								<option>Constitutionele monarchie</option>
								<option>Presidentiële republiek</option>
								<option>Dictatuur</option>
								<option>Absolute monarchie</option>
								<option>Theocratie</option>
								<option>Parlementaire republiek</option>
								<option>Mislukte staat</option>
							</select>
						</div>
					</div>
				</div>

				<div id="quizMelding" class="alert alert-success" role="alert" style="display: none;">Volledig juist, proficiat!</div>

				<button class="btn btn-primary" id="btnCheckQuiz">Controleren</button>
				<a id="afterQuiz" href="/onze-situatie/" class="btn btn-primary margin-left" style="display: none;">Volgende</a>

</form>



			<!-- <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1">
  <label class="form-check-label" for="inlineRadio1">Constitutionele monarchie</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2">
  <label class="form-check-label" for="inlineRadio2">Presidentiële republiek) </label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3">
  <label class="form-check-label" for="inlineRadio3">Dictatuur</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="4">
  <label class="form-check-label" for="inlineRadio1">Absolute monarchie</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="5">
  <label class="form-check-label" for="inlineRadio2">Theocratie</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="6">
  <label class="form-check-label" for="inlineRadio3">Parlementaire republiek</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="7">
  <label class="form-check-label" for="inlineRadio3">Mislukte staat</label>
</div> -->



</div>
</div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script type="text/javascript">
	$( document ).ready(function() {
		$("#btnCheckQuiz").click(function(e){
			e.preventDefault();
			var melding = "";
			$(".quiz-sel select").each(function(){
				var sel = $(this)[0];
				var answer = $(sel).data(answer).answer;
				var selected = $(sel).val();

				// console.log(answer);
				
				if(answer == selected){
					// console.log("juist");
					//var melding = "Volledig juist, proficiat!";
					//$(sel).parent().parent().append('<div class="alert alert-success" role="alert">Juist!</div>');
				}else{
					// console.log("fout");
					melding = "Er is was iets verkeerd, bekijk de juiste oplossing en ga verder.";
					$(sel).parent().parent().append('<div class="alert alert-warning" role="alert">Fout, dit is het juiste antwoord: <strong>'+answer+'</strong></div>');
				}
				
			});
			if(melding != ""){
				$("#quizMelding").removeClass('alert-success');
				$("#quizMelding").addClass('alert-warning');
				$("#quizMelding").html(melding);
			}
			$("#quizMelding").show();
			$("#afterQuiz").show();
		});
	})
</script>
</body>
</html>