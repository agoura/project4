<!DOCTYPE html>
<html>
<head>
	<title>Politieke Organisatiestructuren</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

	<header>
		<nav>
			<ul>
				<li><a href="/">Start</a></li>
				<li><a href="/organisatiestructuren/">Organisatiestructuren</a></li>
				<li><a href="/onze-situatie/">Onze situatie</a></li>
				<li><a href="/verkiezingen/">Verkiezingen</a></li>
				<li><a href="/zelf-aan-de-slag/">Zelf aan de slag</a></li>
				<li><a href="/bijlagen/">Bijlagen</a></li>
				<li><a href="/help/">Help</a></li>
			</ul>
		</nav>
		<h1>Verkiezingen</h1>
	</header>

	<div class="container" id="intro">
		<div class="row">
			<div class="col-sm-12 block block-small-bottom">
				<h1>Waarom?</h1>
				<p>In België worden politieke verkiezingen georganiseerd om de vertegenwoordigers (van het volk) voor wetgevende organen aan te duiden, er wordt niet gekozen voor uitvoerende organen of functies.<br/>
In België kunt u deelnemen aan vijf verschillende verkiezingen. 
</p>
<table class="table table-bordered">
	<thead>
		<tr>
      <th scope="col">Europees</th>
      <th scope="col">Federaal</th>
      <th scope="col">Regionale verkiezingen</th>
      <th scope="col">Provinciale verkiezingen</th>
      <th scope="col">Gemeentelijke verkiezingen</th>
    </tr>
	</thead>
	  <tbody>
    <tr>
      <td scope="row">Europees niveau</td>
      <td>Volksvertegen-woordigers de kamer </td>
      <td>Parlementen</td>
      <td>Provincieraad</td>
      <td>Gemeenteraad</td>
    </tr>
    <tr>
      <td scope="row">5-jaarlijks</td>
      <td>5-jaarlijks</td>
      <td>5-jaarlijks</td>
      <td>6-jaarlijks</td>
      <td>6-jaarlijks</td>
    </tr>
    <tr>
      <td scope="row">26/05/2019</td>
      <td>26/05/2019</td>
      <td>26/05/2019</td>
      <td>14/10/2018</td>
      <td>14/10/2018</td>
    </tr>
  </tbody>
</table>
<p>Na de samenvallende verkiezingen van 25 mei 2014 zullen de federale verkiezingen om de 5 jaar plaatsvinden in plaats van 4 jaar. De deelstaten krijgen ook het recht om na de verkiezingen van 2014 de datum van de regionale verkiezingen en de duur van de legislatuur aan te passen. Vermoedelijk zullen de volgende federale en regionale verkiezingen opnieuw samenvallen met de Europese verkiezingen op zondag 26 mei 2019.</p>
<div class="alert alert-primary" role="alert">
Wist je dat? Wanneer je blanco stemt, je voor de meerderheid stemt?
</div>
<div class="alert alert-primary" role="alert">
Wist je dat? We een stemplicht hebben en geen stemrecht? 
</div>
<div class="alert alert-primary" role="alert">
Wist je dat? Je beboet kan worden wanneer je niet komt opdagen? 
</div>

<a href="/zelf-aan-de-slag/" class="btn btn-primary">Volgende</a>

			</div>
		</div>
	</div>

	



	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
	<script type="text/javascript">
		$( window ).on( "load", function() {
		// $("#kaart iframe #screenshot").trigger("click");
		// var $iframe = $("#maphub").contents();
		// $iframe.find('#screenshot').trigger("click");
	})
</script>
</body>
</html>