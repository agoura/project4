<!DOCTYPE html>
<html>
<head>
	<title>Politieke Organisatiestructuren</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

	<header>
		<nav>
			<ul>
				<li><a href="/">Start</a></li>
				<li><a href="/organisatiestructuren/">Organisatiestructuren</a></li>
				<li><a href="/onze-situatie/">Onze situatie</a></li>
				<li><a href="/verkiezingen/">Verkiezingen</a></li>
				<li><a href="/zelf-aan-de-slag/">Zelf aan de slag</a></li>
				<li><a href="/bijlagen/">Bijlagen</a></li>
				<li><a href="/help/">Help</a></li>
			</ul>
		</nav>
		<h1>Help</h1>
	</header>

	<div class="container" id="intro">
		<div class="row">
			<div class="col-sm-12 block block-small-bottom">
				<ul>
					Bij onzekerheden of problemen kunnen jullie mij altijd contacteren via e-mail: <a href="mailto:hannah.vanhove@ugent.be">hannah.vanhove@ugent.be</a>
				</ul>
			</div>
		</div>
	</div>


</div>
</div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>